'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API: '"http://10.43.169.201:8000"'
  // BASE_API: '"https://api.auauz.net"'
})
